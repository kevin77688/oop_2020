.PHONY: directories clean
CC=g++
CFLAGS=-std=c++17
LIBS=-lgtest -lpthread
OBJ=obj
BIN=bin
SRC=src
TEST=test

all: clean directories $(BIN)/dot

$(BIN)/dot: $(SRC)/main.cpp $(SRC)/dot.h
	$(CC) $(CFLAGS) -o $@ $<

$(BIN)/customTest: $(SRC)/test.cpp
	$(CC) -pthread $(CFLAGS) -o $@ $<

$(BIN)/ut_all: $(OBJ)/ut_main.o
	$(CC) $(CFLAGS) -o $@ $(OBJ)/ut_main.o $(LIBS)

$(OBJ)/ut_main.o: $(TEST)/ut_all.cpp
	$(CC) $(CFLAGS) -c $< -o $@

directories:
	mkdir -p bin obj

clean:
	rm -rf $(OBJ) $(BIN)

stat:
	wc $(SRC)/*.h $(SRC)/*.cpp $(TEST)/*.h $(TEST)/*.cpp
