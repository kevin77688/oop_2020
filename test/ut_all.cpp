#include <gtest/gtest.h>

#include "../src/dot.h"

int main(int argc, char * argv[]) {
  testing::InitGoogleTest( &argc, argv);
  return RUN_ALL_TESTS();
}

TEST(InnerProduct, First) {
  double a[2] = {1, 1};
  double b[2] = {2, 3};
  ASSERT_EQ(5,dot(a, 2, b, 2));
}

TEST(InnerProduct, Exception) {
  double a[3] = {1, 1, 1};
  double b[2] = {2, 3};
  try {
    dot(a, 3, b, 2);
    FAIL() << "should'nt be here";
  }
  catch(std::string message) {
    ASSERT_EQ(std::string("undefined"), message);
  }
}

TEST(InnerProduct, Vector) {
  double a[3] = {1, 2, 3};
  // Vector v(a, 3);
  // ASSERT_EQ(3, v.dim());
  ASSERT_NEAR(10,9,2);
}
