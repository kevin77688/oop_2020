using namespace std;

#include <iostream>
#include <thread>
#include <vector>
#include <future>
#include <string>

int checkPrime(int a)
{
        int c;

        for (c = 2; c <= a - 1; c++)
                if (a%c == 0)
                        break;
        if (c == a) {
                string s = ' ' + to_string(a) + ' ';
                cout << s;
        }

}

int main()
{
        int input;
        // vector<int> primeNumber;
        vector<thread> threads;

        cin >> input;
        for(int i = 0; i < input; i++) {
                threads.push_back(thread(checkPrime, i));
        }
        for(int i = 0; i < threads.size(); i++) {
                threads[i].join();
        }
}
